# Search and Replace

- This java project aim to **search** and **replace** text phrases in regular text documents and xml documents.
- The program read from **standard input** and write to **standard output**.
- The program accepts three command line parameters:
    - Type: the type of data sent on standard input (`xml` or `text`)
    - Search string 
    - Replace string

## Class diagram design

![Class Diagram](./images/ClassDiagram.png)


## Build the project

To build the java project with `Gradle` and generate the jar file named *searchreplace.jar*, run the command line below

```bash
./gradlew jar
```

## Examples of use cases

- Read the content of the text file from `manifesto.txt` using standard input and search for `customer` and replace it with `client` then write the updated content in` restult.txt` using the standard output.

```bash
 java -jar build/libs/searchreplace.jar txt customer client  < build/libs/manifesto.txt > build/libs/result.txt
```

- Read the content of the XML file from `configuration.xml` using standard input and search for `trace` in the XML attribute values and replace it with `error`, then write the updated XML content in `result.xml` using standard output.

```bash
java -jar build/libs/searchreplace.jar xml trace error < build/libs/configuration.xml > build/libs/result.xml
```
## Unit Tests

There are three main unit tests:

- ProcessorFactoryTest
- TextProcessorTest
- XmlProcessorTest

To run all unit test cases, execute the command line below

```bash
./gradlew test
```

## Licence
MIT
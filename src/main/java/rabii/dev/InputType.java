package rabii.dev;

/**
 * Enum class for input types
 *
 * @author Rabii Elbeji
 * @since 1.0.0
 */
public enum InputType {
    TXT,
    XML
}

package rabii.dev;

import java.util.logging.Logger;

/**
 * TextProcessor class
 *
 * @author Rabii Elbeji
 * @since 1.0.0
 */
public class TextProcessor implements IProcessor {

    // Logger instance
    Logger logger = Logger.getLogger(TextProcessor.class.getSimpleName());

    @Override
    public void searchAndReplace(String inputTextString,
                                String searchString,
                                String replaceString) {

        logger.info("Start Text Processing.");

        System.out.println(inputTextString.replace(searchString, replaceString));

        logger.info("Finish Text Processing.");
    }
}

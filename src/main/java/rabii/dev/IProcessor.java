package rabii.dev;

/**
 * Processor Interface
 *
 * @author Rabii Elbeji
 * @since 1.0.0
 */
public interface IProcessor {

    /**
     * Parse the standard input and replace the searchable string
     *
     * @param inputString   Input string
     * @param searchString  String to be changed
     * @param replaceString Replaceable String
     */
    void searchAndReplace(String inputString,
                         String searchString,
                         String replaceString);

}

package rabii.dev;

import java.util.logging.Logger;

/**
 * Provides processor instantiation based on the input type
 *
 * @author Rabii Elbeji
 * @since 1.0.0
 */
public class ProcessorFactory {

    // logger instance
    Logger logger = Logger.getLogger(ProcessorFactory.class.getSimpleName());

    /**
     * Get a Processor instance
     *
     * @param type Input type
     * @return Parser
     */
    public IProcessor getProcessor(String type) throws Exception {

        // init input type variable
        InputType inputType;

        try{
            inputType = InputType.valueOf(type.toUpperCase());
        } catch (Exception e){
            logger.severe("Cannot create processor for \"" + type + "\" type. Unsupported input type!");
            logger.info("Currently supported input types are: TXT or XML");
            throw new Exception("Cannot create processor for \"" + type + "\" type. Unsupported input type!");
        }

        // Create parser instance based on the input type
        switch (inputType) {
            case TXT:
                return new TextProcessor();
            case XML:
                return new XmlProcessor();
            default:
                logger.severe("Unknown error, no processor instance created!");
                throw new Exception("Unknown error, no processor instance created!");
        }
    }

}

package rabii.dev;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.logging.Logger;

/**
 * XmlProcessor class
 *
 * @author Rabii Elbeji
 * @since 1.0.0
 */
public class XmlProcessor implements IProcessor {

    // logger instance
    Logger logger = Logger.getLogger(TextProcessor.class.getSimpleName());

    @Override
    public void searchAndReplace(String inputXmlString, String searchString, String replaceString) {
        try {

            logger.info("Start XML processing.");

            // Build the doc from the XML file
            Document doc = buildDocument(inputXmlString);

            // Locate the node(s) with xpath and replace attribute values if matched
            XPath xpath = XPathFactory.newInstance().newXPath();
            NodeList nodes = (NodeList) xpath.evaluate("//*[contains(@*, '" + searchString + "')]", doc, XPathConstants.NODESET);
            for (int i = 0; i < nodes.getLength(); i++) {
                Node value = nodes.item(i).getAttributes().item(0);
                String val = value.getNodeValue();
                value.setNodeValue(val.replaceAll(searchString, replaceString));
            }

            // write to the standard output
            writeDocument(doc);

            logger.info("Finish XML processing.");

        } catch (IOException | XPathExpressionException | TransformerException | ParserConfigurationException | SAXException e) {
            logger.severe(e.getMessage());
        }
    }


    /**
     * Build Document object from input string
     *
     * @param content Input string
     * @return Document object
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     */
    private Document buildDocument(String content) throws ParserConfigurationException, IOException, SAXException {
        Document doc = null;
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        doc = builder.parse(new InputSource(new StringReader(content.replaceAll("(?:>)(\\s*)<", "><")))); // Remove all whitespace between every XML element
        return doc;
    }

    /**
     * Save Document to standard output
     *
     * @param doc Input xml document object
     * @throws TransformerException
     * @throws UnsupportedEncodingException
     */
    private void writeDocument(Document doc) throws TransformerException, UnsupportedEncodingException {

        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        transformer.transform(new DOMSource(doc),
                new StreamResult(new OutputStreamWriter(System.out)));
    }

}

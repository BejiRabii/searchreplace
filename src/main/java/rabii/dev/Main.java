package rabii.dev;

import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.logging.Logger;

/**
 * Main Class to perform search and replace for a given string
 *
 * @author Rabii Elbeji
 * @since 1.0.0
 */
public class Main {

    /**
     * Main function
     *
     * @param args Three possible args (type, Search String, Replace String)
     */
    public static void main(String[] args) throws Exception {

        // logger instance
        Logger logger = Logger.getLogger(Main.class.getSimpleName());

        if (args.length != 3) {
            logger.severe("Expected arguments are \"Type\", \"Search String\", \"Replace String\"");
            throw new Exception("Expected arguments are \"Type\", \"Search String\", \"Replace String\"");
        }

        // Read command line args
        String type = args[0];
        String searchString = args[1];
        String replaceString = args[2];

        logger.info(String.format("Type: %s, Search String: %s, Replace String: %s%n", type, searchString, replaceString));

        // Assert that acquired command line args aren't null
        Objects.requireNonNull(type);
        Objects.requireNonNull(searchString);
        Objects.requireNonNull(replaceString);


        // Read from standard input using BufferedInputStream
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length;
        while ((length = (System.in).read(buffer)) != -1) {
            baos.write(buffer, 0, length);
        }

        logger.info("Start Search&Replace Process");

        // Perform search and replace process on the standard input
        ProcessorFactory parserFactory = new ProcessorFactory();
        IProcessor processor = parserFactory.getProcessor(type);
        processor.searchAndReplace(baos.toString(StandardCharsets.UTF_8), searchString, replaceString);

        logger.info("Finish Search&Replace Process");

    }

}

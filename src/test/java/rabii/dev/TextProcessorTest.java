package rabii.dev;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Scanner;

/**
 * TextProcessor Test
 *
 * @author Rabii Elbeji
 * @since 1.0.0
 */
public class TextProcessorTest {

    @Test
    public void searchAndReplaceString_inTextContent_shouldPass() {

        //init variables
        String inputText = "Our highest priority is to satisfy the customer\n" +
                "through early and continuous delivery\n" +
                "of valuable software.\n" +
                "Welcome changing requirements, even late in\n" +
                "development. Agile processes harness change for\n" +
                "the customer's competitive advantage.";

        String expectedOutput = "Our highest priority is to satisfy the client\n" +
                "through early and continuous delivery\n" +
                "of valuable software.\n" +
                "Welcome changing requirements, even late in\n" +
                "development. Agile processes harness change for\n" +
                "the client's competitive advantage.";



        // init TextProcessor class
        TextProcessor textProcessor = new TextProcessor();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        PrintStream old = System.out;
        System.setOut(ps);

        textProcessor.searchAndReplace(inputText, "customer", "client");

        System.out.flush();
        System.setOut(old);

        String outputString = baos.toString().trim();

        Assertions.assertEquals(expectedOutput, outputString);

    }

}

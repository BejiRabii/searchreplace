package rabii.dev;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * ProcessorFactory Test
 *
 * @author Rabii Elbeji
 * @since 1.0.0
 */
public class ProcessorFactoryTest {

    @Test
    public void testParserInitiation_withTextFormat_shouldPass() throws Exception {
        ProcessorFactory processorFactory = new ProcessorFactory();
        IProcessor processor = processorFactory.getProcessor("txt");
        Assertions.assertTrue(processor instanceof TextProcessor);
    }

    @Test
    public void testParserInitiation_withXMLFormat_shouldPass() throws Exception {
        ProcessorFactory processorFactory = new ProcessorFactory();
        IProcessor processor = processorFactory.getProcessor("xml");
        Assertions.assertTrue(processor instanceof XmlProcessor);
    }

    @Test
    public void testParserInitiation_withUnsupportedFormat_shouldThrowException()  {

        String expectedErrorMessage = "Cannot create processor for \"pdf\" type. Unsupported input type!";
        String actualErrorMessage = "";
        Exception exception = null;
        IProcessor processor = null;

        ProcessorFactory processorFactory = new ProcessorFactory();

        try {
            processor = processorFactory.getProcessor("pdf");
        } catch (Exception e) {
            exception = e;
            actualErrorMessage = e.getMessage();
        }

        Assertions.assertNull(processor);
        Assertions.assertNotNull(exception);
        Assertions.assertEquals(expectedErrorMessage,  actualErrorMessage);
    }

}

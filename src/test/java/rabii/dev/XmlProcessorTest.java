package rabii.dev;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * XmlProcessor Test
 *
 * @author Rabii Elbeji
 * @since 1.0.0
 */
public class XmlProcessorTest {

    @Test
    public void searchAndReplaceAttributeValues_inXmlContent_shouldPass() {

        //init variables
        String inputXML = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
                "<configuration>\n" +
                "    <properties>\n" +
                "        <!-- Uncomment the following to enable the profiler -->\n" +
                "        <!-- <profiler mode=\"trace\"/> -->\n" +
                "        <log level=\"trace\">\n" +
                "            <file name=\"trace-20180101.log\"/>\n" +
                "        </log>\n" +
                "        <comment>Level can be either \"trace\", \"info\" or \"error\".</comment>\n" +
                "    </properties>\n" +
                "</configuration>";

        String expectedOutput = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
                "<configuration>\n" +
                "    <properties>\n" +
                "        <!-- Uncomment the following to enable the profiler -->\n" +
                "        <!-- <profiler mode=\"trace\"/> -->\n" +
                "        <log level=\"error\">\n" +
                "            <file name=\"error-20180101.log\"/>\n" +
                "        </log>\n" +
                "        <comment>Level can be either \"trace\", \"info\" or \"error\".</comment>\n" +
                "    </properties>\n" +
                "</configuration>";


        // init XmlProcessor class
        XmlProcessor xmlProcessor = new XmlProcessor();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        PrintStream old = System.out;
        System.setOut(ps);

        xmlProcessor.searchAndReplace(inputXML, "trace", "error");

        System.out.flush();
        System.setOut(old);

        String outputXMLString = baos.toString().trim();

        Assertions.assertEquals(expectedOutput, outputXMLString);
    }

}
